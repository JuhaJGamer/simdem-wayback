import express from 'express';
import Db from '../database'
import _ from 'lodash';

var router = express.Router();

router.get('/:date', (req, res, next) => {
    let date = new Date(req.params.date);
    Db.openRepo().then((repo) => {
            return Db.getTreeOnDate(repo, 'master', date);
        }).then((tree) => {
            return Db.getLaws(tree);
        }).then((laws) => {
            console.log(laws);
            let titles = _.map(laws, law => law.title);
            res.json(titles);
        }).catch(next);
});

export default router;
