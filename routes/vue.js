import express from 'express';
import history from 'connect-history-api-fallback';
import _ from 'lodash';

var router = express.Router();

router.use(express.static('vue/dist'));
router.use(history({
    index: '/',
    rewrites: [
         { from: /\/api.*/, to: _.identity },
    ]
}));

export default router;
