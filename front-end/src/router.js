import Vue from 'vue';
import VueRouter from 'vue-router';
import LawExplorer from './components/LawExplorer.vue'
import Law from './components/Law.vue';
import MainView from './components/MainView.vue';

Vue.use(VueRouter);

const routes = [
  {
    path:"/:tab/:date",
    component: MainView,
    props: true,
    children: [
      {
        path: ":path+",
        components: { default: Law, leftcard: Law },
      },
      {
        path: "",
        components: { default: LawExplorer, leftcard: Law },
      },
    ],
  },
  {
    path:"/:tab",
    redirect: to => 
      `${to.params.tab}/${encodeURIComponent(new Date().toISOString())}`,
  },
  {
    path:'/',
    redirect:'/laws',
  },
];

export default new VueRouter({
  routes,
  mode: "history",
});
