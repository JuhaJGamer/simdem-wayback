import Git from 'nodegit'
import _ from 'lodash'
import {exists} from 'fs';

import {union} from './utils'

const repoPath = './law-repo';
const openRepo = () => {
    let p = new Promise((resolve, reject) => {
        try {
            Git.Repository.open(repoPath).then(resolve).catch(reject);
        } catch (err) {
            console.err("of");
            reject(err);
        }
    });
    return p;
}
const getCommitHistory = (commit) => {
    console.log("resolving history");
    let hist = commit.history(),
        p = new Promise((resolve, reject) => {
            hist.on('end', resolve);
            hist.on('error', reject);
        });
    hist.start();
    return p;
};
const  newestCommitBefore = (commits, date) => {
    const lin = _.find(commits, c => +c.date() <= +date)
    return lin;
}
const getTreeOnDate = (repo, branch, date) => {
    console.log("getTreeOnDate");
    return repo.getBranchCommit(branch).then((commit) => {
            return getCommitHistory(commit);
        }).then((commits) => {
            return newestCommitBefore(commits, date).getTree();
        });
}
const entryIsLaw = (treeEntry) => {
    const truths = [
        treeEntry.isFile(),
        /.*\.json$/.test(treeEntry.name()),
        /^law\/.*$/.test(treeEntry.path()),
    ];
    console.log(truths);
    return _.every(truths);
}
const entryToLaw = (entry) => {
    console.log("getting blob....");
    return entry.getBlob().then((blob) => {
            return JSON.parse(blob.content().toString());
        });
}


const collectAllEntries = (tree) => {
    let walker = tree.walk(),
        p = new Promise((resolve, reject) => {
            walker.on('entry', console.log);
            walker.on('end', trees => resolve(trees));
            walker.on('error', reject);
        });
    walker.start();
    return p;
}
const getLaws = (tree) => {
    console.log("parsing laws");
    return collectAllEntries(tree).then((entries) => {
        console.log(entries);
        let laws = _.filter(entries, entryIsLaw);
        console.log(laws);
        return Promise.all(_.map(laws, entryToLaw));
    });
}

export default {openRepo, getCommitHistory, getLaws, getTreeOnDate};

